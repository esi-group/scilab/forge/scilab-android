#!/bin/bash

# Author:  Guillaume Mazoyer <respawneral@gmail.com>
# License: see Scilab license

set -e
set -o nounset

PREFIX="/data/local/usr"
HOST="arm-linux-gnueabi"
JOBS=${JOBS:="-j2"}

mkdir -p files
pushd files

# Download if not already here
if [ ! -e scilab-5.4.0-beta-2-src.tar.gz ]; then
  wget http://www.scilab.org/download/5.4.0-beta-2/scilab-5.4.0-beta-2-src.tar.gz
fi

popd

# Extract
rm -rf temp/scilab-5.4.0-beta-2
tar xzf files/scilab-5.4.0-beta-2-src.tar.gz -C temp

# Patch to avoid a compilation failure
# This needs to be investigated anyway
# patch temp/scilab-5.4.0-beta-1/modules/history_manager/src/c/getCommentDateSession.c \
#  < patches/scilab-build.patch
pushd temp/scilab-5.4.0-beta-2

# "Boss can I take a break? It's compiling... seriously"
PATH="/data/local/usr/bin:${PATH}" CFLAGS="-I/data/local/usr/include/ncurses/ -I/data/local/usr/include/" \
  CPPFLAGS="-I/data/local/usr/include" LDFLAGS="-L/data/local/usr/lib" \
  ./configure --host=${HOST} \
  --prefix=${PREFIX} --without-gui --without-javasci --without-jdk \
  --disable-build-help --without-scicos --without-xcos --without-ocaml \
  --without-modelica --without-fftw --without-pvm --without-umfpack \
  --without-matio --without-tk --without-openmp --disable-build-localization \
  --without-arpack-ng --with-blas-library=/data/local/usr/lib \
  --with-lapack-library=/data/local/usr/lib \
  --with-hdf5-include=/data/local/usr/include \
  --with-hdf5-library=/data/local/usr/lib
make all ${JOBS}
make install
popd

exit 0
