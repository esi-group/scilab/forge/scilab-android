#!/bin/bash

# Author:  Guillaume Mazoyer <respawneral@gmail.com>
# License: see Scilab license

# This script requires a build of Scilab on ARM so it can use some libraries
# files related to modules, etc. Needed files will be searched in the chroot.

set -e
set -o nounset

ANDROID_SDK="/home/guillaume/Development/android-sdk"
export PATH="${PATH}:${ANDROID_SDK}/tools:${ANDROID_SDK}/platform-tools"

# To be able to develop without root rights
OWNER="guillaume"

# Target is Android 4.0
TARGET="android-15"
NAME="scilab-android"
ACTIVITY="ScilabMain"
PACKAGE="org.scilab"

# chroot with ARM toolchain
CHROOT="/opt/gsoc-chroot"

if [[ $# -gt 0 ]] && [[ "${1}" == "reset" ]]; then
  # Remove previous project
  rm -rf ${NAME}

  # Create the project
  android create project --target ${TARGET} --path ${NAME} \
    --activity ${ACTIVITY} --package ${PACKAGE}
fi

pushd ${NAME}

# Copy libs
rm -rf jni/lib
mkdir -p jni/lib
cp -r ${CHROOT}/data/local/usr/lib/* jni/lib/

pushd jni/lib

# Remove symlinks
find . -type l -exec rm -f {} \;
mv scilab/* .
rm -rf scilab
rm -rf pkgconfig
rm -rf gettext

# Remove useless files
rm -f *.o
rm -f *.la
rm -f xml2Conf.sh

# Rename all files to keep only the .so extension
for i in *.so.*; do
  FILE=${i%%.so*}
  mv ${i} ${FILE}.so
done

# Fix some .so files
cp ${CHROOT}/usr/arm-linux-gnueabi/lib/libc.so.6 libc.so
cp ${CHROOT}/usr/arm-linux-gnueabi/lib/libpthread.so.0 libpthread.so

# Manually rename files with name longer than the symlink's
mv libm-2.15.so libm.so

for i in *.so; do
  # Try to generate fixed .so files
  # This could fucked up files due to the use of 'sed', the null-character \x0
  # is used to preserve the size of the string without changing the string
  # itself
  sed -i 's/libc.so.6/libc.so\x0\x0/g' ${i}
  sed -i 's/ld-linux.so.3/libld.so\x0\x0\x0\x0\x0/g' ${i}
  sed -i 's/libgfortran.so.3/libgfortran.so\x0\x0/g' ${i}
  sed -i 's/libm.so.6/libm.so\x0\x0/g' ${i}
  sed -i 's/libgcc_s.so.1/libgcc_s.so\x0\x0/g' ${i}
  sed -i 's/libscifunctions.so.5/libscifunctions.so\x0\x0/g' ${i}
  sed -i 's/libxml2.so.2/libxml2.so\x0\x0/g' ${i}
  sed -i 's/libscicall_scilab.so.5/libscicall_scilab.so\x0\x0/g' ${i}
  sed -i 's/libscilocalization.so.5/libscilocalization.so\x0\x0/g' ${i}
  sed -i 's/libscicompletion.so.5/libscicompletion.so\x0\x0/g' ${i}
  sed -i 's/libscihistory_manager.so.5/libscihistory_manager.so\x0\x0/g' ${i}
  sed -i 's/libpcre.so.1/libpcre.so\x0\x0/g' ${i}
  sed -i 's/libscidynamiclibrary.so.5/libscidynamiclibrary.so\x0\x0/g' ${i}
  sed -i 's/libscixml.so.5/libscixml.so\x0\x0/g' ${i}
  sed -i 's/libsciexternal_objects.so.5/libsciexternal_objects.so\x0\x0/g' ${i}
  sed -i 's/libscipreferences-cli.so.5/libscipreferences-cli.so\x0\x0/g' ${i}
  sed -i 's/libscigui-disable.so.5/libscigui-disable.so\x0\x0/g' ${i}
  sed -i 's/libscigraphics-disable.so.5/libscigraphics-disable.so\x0\x0/g' ${i}
  sed -i 's/libsciconsole-minimal.so.5/libsciconsole-minimal.so\x0\x0/g' ${i}
  sed -i 's/libscijvm-disable.so.5/libscijvm-disable.so\x0\x0/g' ${i}
  sed -i 's/libscigraphic_objects-disable.so.5/libscigraphic_objects-disable.so\x0\x0/g' ${i}
  sed -i 's/libsciui_data-disable.so.5/libsciui_data-disable.so\x0\x0/g' ${i}
  sed -i 's/libscihistory_browser-disable.so.5/libscihistory_browser-disable.so\x0\x0/g' ${i}
  sed -i 's/libscimalloc.so.5/libscimalloc.so\x0\x0/g' ${i}
  sed -i 's/libscilibst.so.5/libscilibst.so\x0\x0/g' ${i}
  sed -i 's/libpthread.so.0/libpthread.so\x0\x0/g' ${i}
  sed -i 's/libdl.so.2/libdl.so\x0\x0/g' ${i}
  sed -i 's/libncurses.so.5/libncurses.so\x0\x0/g' ${i}
  sed -i 's/libscilab-cli.so.0/libscilab-cli.so\x0\x0/g' ${i}
done

# Since ld does not start with lib it is not considered as a shared library by
# Android (all libs must have a name like `lib%s.so') see:
# system/core/include/arch/linux-sh/AndroidConfig.h => OS_SHARED_LIB_FORMAT_STR
mv ld-2.15.so libld.so

# Generate the Android.mk file for NDK build
echo -e "LOCAL_PATH := \$(call my-dir)\n" > Android.mk

for i in *.so; do
  MODULE=${i%%.*}
  if [[ "${MODULE}" == lib* ]]; then
    MODULE=${MODULE##lib}
  fi

  # Module stdc++ is already defined in the Android system
  if [[ ! "${MODULE}" == "stdc++"  ]]; then
    echo -e "include \$(CLEAR_VARS)" >> Android.mk
    echo -e "LOCAL_MODULE := ${MODULE}" >> Android.mk
    echo -e "LOCAL_SRC_FILES := ${i}" >> Android.mk
    echo -e "include \$(PREBUILT_SHARED_LIBRARY)\n" >> Android.mk
  fi
done

popd

# Copy assets
rm -rf assets
mkdir -p assets
cp -r ${CHROOT}/data/local/usr/share/scilab/* assets/

popd

# Change owner, developing as root would be silly
chown -R ${OWNER}:${OWNER} ${NAME}

exit 0
