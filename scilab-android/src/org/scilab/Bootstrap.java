package org.scilab;

import android.app.Activity;
import android.app.NativeActivity;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.util.Log;

public final class Bootstrap extends NativeActivity {
	private static final String TAG = "ScilabBootstrap";

	static {
		/*
		 * Load the bootstrap library.
		 */
		System.loadLibrary("scilab-bootstrap");
	}

	public static native boolean setup(int scilabMainPointer,
			Object scilabMainArgument, int scilabMainDelay);

	private static native boolean setup(String dataDir, String apkFile,
			String[] ldLibraryPath);

	public static native int dlopen(String library);

	public static native int dlsym(int handle, String symbol);

	public static native int getpid();

	public static native void system(String command);

	public static native void setEnvironment(String key, String value);

	public static void setup(Activity activity) {
		final ApplicationInfo ai;
		final String dataDir;
		final String[] llpa;
		String llp;

		ai = activity.getApplicationInfo();
		dataDir = ai.dataDir;

		Log.i(TAG, String.format("dataDir=%s\n", dataDir));

		llp = System.getenv("LD_LIBRARY_PATH");
		if (llp == null) {
			llp = "/vendor/lib:/system/lib";
		}

		llpa = llp.split(":");
		if (!setup(dataDir, activity.getApplication().getPackageResourcePath(),
				llpa)) {
			return;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		final String mainLibrary;
		final int scilabLib, scilabMain;

		/*
		 * Set the Scilab home directory.
		 */
		setEnvironment("SCI", "/storage/scilab");

		setup(this);

		/*
		 * Library to load.
		 */
		mainLibrary = "/data/data/org.scilab/lib/libscilab.so";

		/*
		 * Load the Scilab "program".
		 */
		scilabLib = dlopen(mainLibrary);

		/*
		 * Look up for the main function.
		 */
		scilabMain = dlsym(scilabLib, "main");

		if (!setup(scilabMain, null, 0)) {
			return;
		}

		/*
		 * This is very important that this instruction must be the last one of
		 * this method. Everything must be set up first.
		 */
		super.onCreate(savedInstanceState);
	}
}
