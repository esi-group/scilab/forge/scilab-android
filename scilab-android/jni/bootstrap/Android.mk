LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

TARGET_ARCH      := arm
TARGET_ARCH_ABI  := armeabi-v7a

# LOCAL_MODULE     := call-scilab
# LOCAL_SRC_FILES  := call-scilab.c
# LOCAL_C_INCLUDES := $(LOCAL_PATH)/../scilab-src/modules/core/includes
# LOCAL_LDLIBS     := -L$(LOCAL_PATH)/../libs/armeabi/scilab/ -lscilab -lscilab-cli
# LOCAL_STATIC_LIBRARIES := libraries
# LOCAL_SHARED_LIBRARIES := libscilab-cli liblapack libgfortran

LOCAL_MODULE           := scilab-bootstrap
LOCAL_SRC_FILES        := scilab_bootstrap.c
LOCAL_LDLIBS           := -llog -landroid
LOCAL_STATIC_LIBRARIES := android_native_app_glue

include $(BUILD_SHARED_LIBRARY)

$(call import-module,android/native_app_glue)
