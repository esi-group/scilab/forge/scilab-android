/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#ifndef SCILAB_BOOSTRAP_H
#define SCILAB_BOOSTRAP_H
#if defined(ANDROID)

#include <jni.h>
#include <dlfcn.h>
#include <dirent.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct scilab_apk_dir scilab_apk_dir;

char** scilab_dlneeds(const char* library);

void* scilab_dlopen(const char* library);

void* scilab_dlsym(void* handle,
                   const char* symbol);

int scilab_dladdr(void* addr,
                  Dl_info* info);

int scilab_dlclose(void* handle);

void* scilab_apkentry(const char* filename,
                      size_t* size);

scilab_apk_dir* scilab_apk_opendir(const char* dirname);

struct dirent* scilab_apk_readdir(scilab_apk_dir* dirp);

int scilab_apk_closedir(scilab_apk_dir* dirp);

int scilab_apk_lstat(const char* path, struct stat* statp);

int scilab_dlcall_argc_argv(void* function,
                            int argc,
                            const char** argv);

JavaVM* scilab_get_javavm(void);

struct android_app* scilab_get_app(void);

#ifdef __cplusplus
}
#endif

#endif // ANDROID
#endif // SCILAB_BOOTSTRAP_H

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
