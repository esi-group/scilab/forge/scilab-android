LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := anl-2
LOCAL_SRC_FILES := libanl-2.15.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := asprintf
LOCAL_SRC_FILES := libasprintf.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := blas
LOCAL_SRC_FILES := libblas.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := BrokenLocale-2
LOCAL_SRC_FILES := libBrokenLocale-2.15.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := c-2
LOCAL_SRC_FILES := libc-2.15.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := cidn-2
LOCAL_SRC_FILES := libcidn-2.15.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := crypt-2
LOCAL_SRC_FILES := libcrypt-2.15.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := c
LOCAL_SRC_FILES := libc.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := dl-2
LOCAL_SRC_FILES := libdl-2.15.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := form
LOCAL_SRC_FILES := libform.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := gcc_s
LOCAL_SRC_FILES := libgcc_s.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := gettextlib-0
LOCAL_SRC_FILES := libgettextlib-0.18.1.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := gettextpo
LOCAL_SRC_FILES := libgettextpo.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := gettextsrc-0
LOCAL_SRC_FILES := libgettextsrc-0.18.1.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := gfortran
LOCAL_SRC_FILES := libgfortran.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := gomp
LOCAL_SRC_FILES := libgomp.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := hdf5_fortran
LOCAL_SRC_FILES := libhdf5_fortran.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := hdf5hl_fortran
LOCAL_SRC_FILES := libhdf5hl_fortran.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := hdf5_hl
LOCAL_SRC_FILES := libhdf5_hl.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := hdf5
LOCAL_SRC_FILES := libhdf5.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := history
LOCAL_SRC_FILES := libhistory.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := lapack
LOCAL_SRC_FILES := liblapack.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := ld
LOCAL_SRC_FILES := libld.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := memusage
LOCAL_SRC_FILES := libmemusage.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := menu
LOCAL_SRC_FILES := libmenu.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := m
LOCAL_SRC_FILES := libm.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := ncurses
LOCAL_SRC_FILES := libncurses.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := nsl-2
LOCAL_SRC_FILES := libnsl-2.15.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := nss_compat-2
LOCAL_SRC_FILES := libnss_compat-2.15.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := nss_dns-2
LOCAL_SRC_FILES := libnss_dns-2.15.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := nss_files-2
LOCAL_SRC_FILES := libnss_files-2.15.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := nss_hesiod-2
LOCAL_SRC_FILES := libnss_hesiod-2.15.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := nss_nis-2
LOCAL_SRC_FILES := libnss_nis-2.15.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := nss_nisplus-2
LOCAL_SRC_FILES := libnss_nisplus-2.15.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := panel
LOCAL_SRC_FILES := libpanel.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := pcprofile
LOCAL_SRC_FILES := libpcprofile.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := pcrecpp
LOCAL_SRC_FILES := libpcrecpp.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := pcreposix
LOCAL_SRC_FILES := libpcreposix.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := pcre
LOCAL_SRC_FILES := libpcre.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := pthread-2
LOCAL_SRC_FILES := libpthread-2.15.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := pthread
LOCAL_SRC_FILES := libpthread.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := readline
LOCAL_SRC_FILES := libreadline.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := resolv-2
LOCAL_SRC_FILES := libresolv-2.15.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := rt-2
LOCAL_SRC_FILES := librt-2.15.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := sciaction_binding-disable
LOCAL_SRC_FILES := libsciaction_binding-disable.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := sciarnoldi
LOCAL_SRC_FILES := libsciarnoldi.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scicall_scilab
LOCAL_SRC_FILES := libscicall_scilab.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scicommons-disable
LOCAL_SRC_FILES := libscicommons-disable.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scicompletion
LOCAL_SRC_FILES := libscicompletion.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := sciconsole-minimal
LOCAL_SRC_FILES := libsciconsole-minimal.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scidoublylinkedlist
LOCAL_SRC_FILES := libscidoublylinkedlist.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scidynamiclibrary
LOCAL_SRC_FILES := libscidynamiclibrary.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := sciexternal_objects
LOCAL_SRC_FILES := libsciexternal_objects.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scifunctions
LOCAL_SRC_FILES := libscifunctions.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scigraphic_export-disable
LOCAL_SRC_FILES := libscigraphic_export-disable.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scigraphic_objects-disable
LOCAL_SRC_FILES := libscigraphic_objects-disable.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scigraphics-disable
LOCAL_SRC_FILES := libscigraphics-disable.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scigui-disable
LOCAL_SRC_FILES := libscigui-disable.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scihashtable
LOCAL_SRC_FILES := libscihashtable.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scihdf5-forceload-disable
LOCAL_SRC_FILES := libscihdf5-forceload-disable.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scihdf5
LOCAL_SRC_FILES := libscihdf5.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scihelptools
LOCAL_SRC_FILES := libscihelptools.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scihistory_browser-disable
LOCAL_SRC_FILES := libscihistory_browser-disable.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scihistory_manager
LOCAL_SRC_FILES := libscihistory_manager.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := sciinterpolation
LOCAL_SRC_FILES := libsciinterpolation.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scijvm-disable
LOCAL_SRC_FILES := libscijvm-disable.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scilab-cli
LOCAL_SRC_FILES := libscilab-cli.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scilab
LOCAL_SRC_FILES := libscilab.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scilibst
LOCAL_SRC_FILES := libscilibst.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scilocalization
LOCAL_SRC_FILES := libscilocalization.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scimalloc
LOCAL_SRC_FILES := libscimalloc.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scimatio
LOCAL_SRC_FILES := libscimatio.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scioptimization
LOCAL_SRC_FILES := libscioptimization.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := sciparallel
LOCAL_SRC_FILES := libsciparallel.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scipreferences-cli
LOCAL_SRC_FILES := libscipreferences-cli.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scirandlib
LOCAL_SRC_FILES := libscirandlib.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := sciscicos
LOCAL_SRC_FILES := libsciscicos.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := sciscinotes-disable
LOCAL_SRC_FILES := libsciscinotes-disable.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scisignal_processing
LOCAL_SRC_FILES := libscisignal_processing.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scisound
LOCAL_SRC_FILES := libscisound.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scispecial_functions
LOCAL_SRC_FILES := libscispecial_functions.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scispreadsheet
LOCAL_SRC_FILES := libscispreadsheet.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scistatistics
LOCAL_SRC_FILES := libscistatistics.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scisymbolic
LOCAL_SRC_FILES := libscisymbolic.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scitclsci
LOCAL_SRC_FILES := libscitclsci.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scitypes-disable
LOCAL_SRC_FILES := libscitypes-disable.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := sciui_data-disable
LOCAL_SRC_FILES := libsciui_data-disable.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := sciumfpack
LOCAL_SRC_FILES := libsciumfpack.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scixcos-disable
LOCAL_SRC_FILES := libscixcos-disable.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := scixml
LOCAL_SRC_FILES := libscixml.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := SegFault
LOCAL_SRC_FILES := libSegFault.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := thread_db-1
LOCAL_SRC_FILES := libthread_db-1.0.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := util-2
LOCAL_SRC_FILES := libutil-2.15.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := xml2
LOCAL_SRC_FILES := libxml2.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := z
LOCAL_SRC_FILES := libz.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := preloadable_libintl
LOCAL_SRC_FILES := preloadable_libintl.so
include $(PREBUILT_SHARED_LIBRARY)

