#!/bin/sh

# Author:  Guillaume Mazoyer <respawneral@gmail.com>
# License: see Scilab license

ANDROID_SDK="/home/guillaume/Development/android-sdk"
export PATH="${PATH}:${ANDROID_SDK}/platform-tools"

adb push gsoc-chroot/data/local/usr /data/local/usr
adb push gsoc-chroot/data/local/scilab.sh /data/local/scilab.sh
adb shell chown -R 10059:2000 /data/local/usr
adb shell chmod 0755 /data/local/scilab.sh
